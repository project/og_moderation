<?php
/**
 * Implements hook_og_permission().
 *
 */
function og_moderation_og_permission() {
  $perms = array();
  $infos = node_type_get_types();
  foreach($infos as $info) {
    if (og_is_group_content_type('node', $info->type)) {
      $perms["view any unpublished " . $info->type . " content"] = array(
        'title' => t('View any unpublished %type_name content', array('%type_name' => $info->name)),
      );
      $perms["access publishing options of " . $info->type . " content"] = array(
        'title' => t('Access publishing options of %type_name content', array('%type_name' => $info->name)),
      );
      $perms["access revisions options of " . $info->type . " content"] = array(
        'title' => t('Access revisions options of %type_name content', array('%type_name' => $info->name)),
      );
    }
  }
  return $perms;
}

function og_moderation_form_alter(&$form, &$form_state, $form_id) {
  if (isset($form['#node_edit_form'])) {
    if ($form['#node_edit_form'] && og_is_group_content_type('node', $form['#bundle'])) {
      if ($gids = og_get_entity_groups('node', $form['#node'])) {
        foreach($gids as $gid) {
          if (og_user_access($gid, "access publishing options of " . $form['#bundle'] . " content")) {
            $form['options']['#access'] = TRUE;
          }
          if (og_user_access($gid, "access revisions options of " . $form['#bundle'] . " content")) {
            $form['revision_information']['#access'] = TRUE;
          }
        }
      }
    }
  }
}

/**
 * Implements hook_node_access_records().
 */
function og_moderation_node_access_records($node) {
  if ($node->status == 0 && $gids = og_get_entity_groups('node', $node)) {
    foreach($gids as $gid) {
      $grants = array();
      $grants[] = array(
        'realm' => 'og_view_any_unpublished_' . $node->type . '_content',
        'gid' => $gid,
        'grant_view' => 1,
        'grant_update' => 0,
        'grant_delete' => 0,
        'priority' => 0,
      );
    }
    return $grants;
  }
}

/**
 * Implements hook_node_grants().
 */
function og_moderation_node_grants($account, $op) {
  $grants = array();
  if ($op == 'view' && $gids = og_get_entity_groups('user', $account)) {
    $types = (empty($types) || $reset == TRUE) ? node_permissions_get_configured_types() : $types;
    foreach($types as $type) {
      foreach ($gids as $gid => $value) {
        if (og_user_access($gid, "view any unpublished $type content")) {
          $grants['og_view_any_unpublished_' . $type . '_content'] = array($gid);
        }
      }
    }
  }
  return $grants;
}

/**
 * Implements hook_menu_alter().
 *
 * Modify menu items defined in other modules (in particular the Node and
 * Module Grants modules).
 */
function og_moderation_menu_alter(&$items) {
  $items['node/%node/revisions']['access callback'] = 'og_moderation_node_revision_access';
  $items['node/%node/revisions']['access arguments'] = array(1);
  $items['node/%node/revisions']['page callback'] = 'og_moderation_node_revision_overview';

  $items['node/%node/revisions/%/view']['access callback'] = 'og_moderation_node_revision_access';
  $items['node/%node/revisions/%/view']['access arguments'] = array(1);

  $items['node/%node/revisions/%/revert']['access callback'] = 'og_moderation_node_revision_access';
  $items['node/%node/revisions/%/revert']['access arguments'] = array(1, 4);
  $items['node/%node/revisions/%/delete']['access callback'] = 'og_moderation_node_revision_access';
  $items['node/%node/revisions/%/delete']['access arguments'] = array(1, 4);
}

function og_moderation_node_revision_access($node, $op = 'view') {
  if ($gids = og_get_entity_groups('node', $node)) {
    foreach ($gids as $gid => $value) {
      if (og_user_access($gid, "access revisions options of " . $node->type . " content")) {
        return TRUE;
      }
    }
  }
  return _node_revision_access($node, $op = 'view');
}

function og_moderation_node_revision_overview($node) {
  drupal_set_title(t('Revisions for %title', array('%title' => $node->title)), PASS_THROUGH);

  $header = array(t('Revision'), array('data' => t('Operations'), 'colspan' => 2));

  $revisions = node_revision_list($node);

  $rows = array();
  $revert_permission = FALSE;
  if ((user_access('revert revisions') || user_access('administer nodes')) && node_access('update', $node)) {
    $revert_permission = TRUE;
  }
  $delete_permission = FALSE;
  if ((user_access('delete revisions') || user_access('administer nodes')) && node_access('delete', $node)) {
    $delete_permission = TRUE;
  }

  if ($gids = og_get_entity_groups('node', $node)) {
    foreach ($gids as $gid => $value) {
      if (og_user_access($gid, "access revisions options of " . $node->type . " content")) {
        $revert_permission = TRUE;
        $delete_permission = TRUE;
        break;
      }
    }
  }

  foreach ($revisions as $revision) {
    $row = array();
    $operations = array();

    if ($revision->current_vid > 0) {
      $row[] = array('data' => t('!date by !username', array('!date' => l(format_date($revision->timestamp, 'short'), "node/$node->nid"), '!username' => theme('username', array('account' => $revision))))
                               . (($revision->log != '') ? '<p class="revision-log">' . filter_xss($revision->log) . '</p>' : ''),
                     'class' => array('revision-current'));
      $operations[] = array('data' => drupal_placeholder(t('current revision')), 'class' => array('revision-current'), 'colspan' => 2);
    }
    else {
      $row[] = t('!date by !username', array('!date' => l(format_date($revision->timestamp, 'short'), "node/$node->nid/revisions/$revision->vid/view"), '!username' => theme('username', array('account' => $revision))))
               . (($revision->log != '') ? '<p class="revision-log">' . filter_xss($revision->log) . '</p>' : '');
      if ($revert_permission) {
        $operations[] = l(t('revert'), "node/$node->nid/revisions/$revision->vid/revert");
      }
      if ($delete_permission) {
        $operations[] = l(t('delete'), "node/$node->nid/revisions/$revision->vid/delete");
      }
    }
    $rows[] = array_merge($row, $operations);
  }

  $build['node_revisions_table'] = array(
    '#theme' => 'table',
    '#rows' => $rows,
    '#header' => $header,
  );

  return $build;
}
